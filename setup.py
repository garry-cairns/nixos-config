#!/home/garry/.nix-profile/bin/python3
"""First time setup following a NixOS install. Creates appropriate symlinks for dotfiles."""

import logging
import subprocess
import sys


DOTFILES = [
    # if anything changes a lot don't symlink to /etc, which is only on rebuild
    'ln -s ~/nixos-config/dotfiles/init.el ~/.emacs.d/init.el',
    'ln -s ~/nixos-config/dotfiles/rofi.rasi ~/.config/rofi/config.rasi',
    'ln -s ~/nixos-config/dotfiles/xmobarrc ~/.xmobarrc',
    'ln -s ~/nixos-config/dotfiles/xmonad.hs ~/.xmonad/xmonad.hs',
    'ln -s ~/nixos-config/dotfiles/xsessions ~/.xsessionrc',
    # unsure if syncthing can be symlinked at /etc so safety first
    'ln -s ~/nixos-config/dotfiles/syncthing.xml ~/.syncthing/config.xml ',
    # everything else can sync to the rebuild (and prob more correct) location
    'ln -s /etc/gitconfig ~/.gitconfig',
    'ln -s /etc/msmtprc ~/.msmtprc',
    'ln -s /etc/notmuch-config ~/.notmuch-config',
    'ln -s /etc/offlineimaprc ~/.offlineimaprc',
]


def symlink(cmd):
    """ Applies the symlink passed in.

    Paramaters
    ----------
    cmd: str
        The symlink to be applied.

    Returns
    -------
    None

    """
    subprocess.run(cmd.split())
    logging.info('Added symlink: {}.'.format(cmd))


def symlink_multiple(cmd_list):
    """Creates symlinks for the commands passed in.
    
    Paramaters
    ----------
    cmd_list: list
        A list of symlink commands to be applied.

    Returns
    -------
    None

    """
    for cmd in cmd_list:
        symlink(cmd)


def main():
    """ Entrypoint and default run configuration. """
    logging.basicConfig(level=logging.INFO)
    logging.info('Creating symlinks.')
    symlink_multiple(DOTFILES)
    logging.info('Finished symlinking.')
    sys.exit(0)


if __name__ == '__main__':
    sys.exit(main())
