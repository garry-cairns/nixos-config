{ config, pkgs, ... }:

{
  # Setup nixpkgs
  nixpkgs.config = {
    allowUnfree = true;
  };

  # List packages installed in system profile. To search by name, run:
  # $ nix-env -qaP | grep wget
  environment.systemPackages = with pkgs;
  let
    RWithPackages = pkgs.rWrapper.override{ packages = with pkgs.rPackages; [
      arm
      bayesplot
      gapminder
      gganimate
      ggplot2
      knitr
      magick
      markovchain
      matlib
      mosaicCalc
      plotly
      pracma
      rstanarm
      rstan
      survey
    ];
  };
  in
  [
    ark
    cabal2nix
    chromium
    cmus
    darktable
    dragon
    evince
    feh
    firefox
    fish
    gcc
    ghostscript
    gimp
    git
    gitflow
    gnumake
    gnupg
    hunspell
    hunspellDicts.en-gb-large
    imagemagick
    keepass
    krita
    lcms2
    marker
    mimeo
    msmtp
    nix-prefetch-git
    notmuch
    offlineimap
    pass
    pdfgrep
    plantuml
    python3
    RWithPackages
    sass
    unzip
    zeal
    zip
    ## Haskell packages
    cabal-install
    ghc
    haskellPackages.hserv
    hlint
    # ## Lisp packages
    #asdf
    #clisp
    #lispPackages.quicklisp
    #lispPackages.quicklisp-to-nix
    ## Python packages
    python312Packages.jupytext
    ## Tex packages
    texlive.combined.scheme-full
  ];
}
