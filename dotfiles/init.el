;; init.el --- Sets up emacs environment
;;; Commentary:

;;; Code:

;; Custom commands
(defun copy-whole-buffer ()
  "Copy entire buffer to clipboard."
  (interactive)
  (clipboard-kill-ring-save (point-min) (point-max))
)

(defun format-buffer ()
  "Function formats code buffers with the appropriate formatter on save."
  (interactive)
  (when (eq major-mode 'haskell-mode)
    (call-process-region (point-max) (point-min) "brittany" t t nil))
  (when (eq major-mode 'python-mode)
    (call-process-region (point-max) (point-min) "black"))
)

(defun get-diary-entry ()
  "Return filename for today's diary entry."
  (let ((daily-name (format-time-string "%Y%m%d")))
    (expand-file-name (concat "~/diary/" daily-name))))

(defun open-diary-entry ()
  "Create or load a diary entry based on today's date."
  (interactive)
  (find-file (get-diary-entry)))

;; Bootstrap `use-package'

(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives
	         '("melpa" . "https://melpa.org/packages/")
)

(package-initialize)

(unless (package-installed-p 'use-package)
	(package-refresh-contents)
	(package-install 'use-package)
)

(require 'use-package)

;; Emacs configuration

(setq coding-system-for-read 'utf-8 )
(setq coding-system-for-write 'utf-8 )
(setq ediff-split-window-function 'split-window-horizontally)
(setq indent-line-function 'insert-tab)
(setq inhibit-startup-message t)
(setq initial-scratch-message ";; Take notes here. <SPACE> fe will edit a new file\n\n")
(setq ispell-program-name "/run/current-system/sw/bin/hunspell")
(setq ispell-local-dictionary "en_GB")
(setq ispell-local-dictionary-alist
      '(("en_GB" "[[:alpha:]]" "[^[:alpha:]]" "[']" nil nil nil utf-8)))
(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)
(add-to-list 'auto-mode-alist '(".*/[0-9]*$" . org-mode))
(add-to-list 'auto-mode-alist '("\\.http\\'" . restclient-mode))

(add-hook 'after-init-hook (lambda () (load-theme 'solarized-light)))

(tool-bar-mode -1)
(savehist-mode 1)

(use-package company
  :ensure t
)

(use-package direnv
 :config
 (direnv-mode))

(use-package eldoc
  :ensure t
)

(use-package flycheck
  :ensure t
  :config
  (global-flycheck-mode)
)

(use-package key-chord
  :ensure t
  :config
  (key-chord-mode 1)
  (setq key-chord-two-keys-delay 0.2)
  (setq key-chord-one-key-delay 0.3)
)

(use-package magit
  :ensure t
)

(use-package projectile
  :ensure t
)

(use-package try
  :ensure t
)

(use-package which-key
  :ensure t
  :init
  (which-key-mode)
  :config
  (setq which-key-sort-order 'which-key-key-order-alpha
        which-key-idle-delay 0.05)
  :diminish (which-key-mode . "Ꙍ")
)

;; Appearance

(use-package solarized-light
  :ensure t
)

(add-to-list 'default-frame-alist '(font . "IBM Plex Mono"))
(custom-theme-set-faces
  'user
  '(fixed-pitch ((t (:family "IBM Plex Mono"))))
  '(variable-pitch ((t (:family "IBM Plex Serif"))))
)

(defface my-green '((t (:foreground "#1f2620" :background "#ccffd4"))) "")
(display-time-mode 1)
(use-package telephone-line
  :ensure t
  :config
  (telephone-line-defsegment datetime ()
    (format-time-string "%Y-%b-%d %H:%M")
  )
  (telephone-line-defsegment buffer-info ()
    (format-mode-line
      (list
       " %b" ;; buffer name
       "%+ " ;; show if buffer is modified
       " %m" ;; major mode
       )
     )
  )
  (telephone-line-defsegment flycheck-info ()
    (format-mode-line
      (list
       flycheck-mode-line
       )
     )
  )
  (telephone-line-defsegment location-info ()
    (format-mode-line
      (list
       "L: %l C: %c"
       )
     )
  )
        ;;
  (setq telephone-line-faces
        '(
          (time . (my-green . my-green))
          (evil . telephone-line-modal-face)
          (accent . (telephone-line-accent-active . telephone-line-accent-inactive))
          (nil . (mode-line . mode-line-inactive))
          )
  )
  (setq telephone-line-evil-use-short-tag t)
  (setq telephone-line-lhs
        '(
          (evil   . (telephone-line-evil-tag-segment))
          (accent . (telephone-line-vc-segment))
          (nil    . (buffer-info))
        )
  )
  (setq telephone-line-rhs
        '(
          (nil    . (flycheck-info))
          (accent . (location-info))
          (time   . (datetime))
        )
  )
  (telephone-line-mode 1)
)
  
;; Commands

(use-package general
  :ensure t
  :config
  (general-define-key
   :states '(normal visual insert emacs)
   :prefix "SPC"
   :non-normal-prefix "C-SPC"
      "b"  '(:ignore t :which-key "buffer")
      "bb" 'switch-to-buffer
      "bd" 'kill-buffer-and-window
      "be" 'eval-buffer
      "bf" 'format-buffer
      "by" 'copy-whole-buffer
      "c"  '(:ignore t :which-key "flycheck-error")
      "cn" 'flycheck-next-error
      "cp" 'flycheck-previous-error
      "do" 'open-diary-entry
      "f"  '(:ignore t :which-key "file")
      "fe" 'find-file
      "fl" 'load-file
      "fs" 'save-buffer
      "g"  '(:ignore t :which-key "git")
      "gs" 'magit-status
      "i"  '(:ignore t :which-key "insert")
      "iu" 'insert-char
      "m"  '(:ignore t :which-key "messaging")
      "mc" 'ement-connect
      "ml" 'ement-room-list
      "mm" 'ement-room-send
      "mr" 'notmuch
      "ms" 'message-send-and-exit
      "o"  '(:ignore t :which-key "org")
      "oa" 'org-agenda
      "ob" 'org-insert-structure-template
      "oc" 'org-capture
      "oe" 'org-set-effort
      "oi" 'org-insert-todo-heading
      "ol" 'org-store-link
      "or" 'org-archive-subtree-default
      "os" 'org-schedule
      "ot" 'org-todo
      "ox" 'org-cycle
      "q"  '(:ignore t :which-key "quit")
      "qq" 'save-buffers-kill-emacs
      "r"  '(:ignore t :which-key "RSS")
      "re" 'elfeed
      "rr" 'elfeed-search-show-entry
      "ru" 'elfeed-update
      "s"  '(:ignore t :which-key "spelling")
      "sn" 'flyspell-goto-next-error
      "t"  '(:ignore t :which-key "toggle-view")
      "tn" 'display-line-numbers-mode
      "ts" 'whitespace-mode
      "w"  '(:ignore t :which-key "window")
      "w1" 'delete-other-windows
      "wh" 'windmove-left
      "wj" 'windmove-down
      "wk" 'windmove-up
      "wl" 'windmove-right
      "wo" 'other-window
      "ws" 'split-window-horizontally
  )
)

(use-package evil
  :ensure t
  :config
  (evil-mode 1)
  (use-package evil-escape
    :ensure t
    :config
    (evil-escape-mode 1)
    (setq-default evil-escape-key-sequence "fd")
    (setq-default evil-escape-delay 0.2)
    (evil-define-key 'normal org-mode-map (kbd "<tab>") #'org-cycle))
    (evil-set-initial-state 'bluetooth-mode 'emacs)
    (evil-set-initial-state 'notmuch-search-mode 'emacs)
    (evil-set-initial-state 'notmuch-show-mode 'emacs)
)

(use-package undo-tree
  :ensure t
  :after evil
  :diminish
  :config
  (evil-mode 1)
  (evil-set-undo-system 'undo-tree)
  (global-undo-tree-mode 1)
)

;; Elfeed

(use-package elfeed
  :ensure t
  :config
  (elfeed-org)
  (use-package elfeed-org
    :ensure t
    :config
    (setq rmh-elfeed-org-files (list "~/org/feeds.org")))
)

(use-package bluetooth
  :ensure t
)

(use-package ement
  :ensure t
  :config
  (setq ement-server-url "https://matrix.org")
  (setq ement-save-session t)
)


(use-package notmuch
  :ensure t
  :config
  (setq message-sendmail-envelope-from 'header)
  (setq smtpmail-smtp-server "smtp.gmail.com"
        smtpmail-smtp-service 25)
  (setq send-mail-function 'smtpmail-send-it)
  (setq notmuch-hello-sections '(notmuch-hello-insert-header notmuch-hello-insert-alltags))
  (evil-add-hjkl-bindings notmuch-search-mode-map 'emacs)
  (evil-add-hjkl-bindings notmuch-show-mode-map 'emacs)
  (add-hook 'notmuch-message-mode-hook 'paragraph-indent-minor-mode)
  (add-hook 'notmuch-message-mode-hook 'turn-off-auto-fill)
  (add-hook 'notmuch-message-mode-hook 'turn-on-flyspell)
  (add-hook 'notmuch-message-mode-hook 'visual-line-mode)
  (add-hook 'notmuch-message-mode-hook 'variable-pitch-mode)
)

;; Programming and writing environments

(use-package org
  :ensure t
  :config
  (add-hook 'org-mode-hook 'turn-on-flyspell)
  (add-hook 'org-mode-hook 'visual-line-mode)
  (add-hook 'org-mode-hook 'variable-pitch-mode)
  (setq org-src-fontify-natively t)
  (custom-theme-set-faces
    'user
    '(org-block ((t (:inherit fixed-pitch))))
    '(org-code ((t (:inherit (shadow fixed-pitch)))))
    '(org-indent ((t (:inherit (org-hide fixed-pitch)))))
    '(org-latex ((t (:inherit (shadow fixed-pitch)))))
    '(org-table ((t (:inherit (shadow fixed-pitch)))))
  )
  (setq org-agenda-files (list "~/org/hobbies.org"
                               "~/org/personal.org"
                               "~/org/reading.org"
                               "~/org/work.org"))
  (eval-after-load 'ox '(require 'ox-koma-letter))
  (with-eval-after-load 'ox-latex
    (add-to-list 'org-latex-classes
                 '("tma"
                   "\\documentclass[a4paper, 12pt]{article}
\\usepackage[english]{babel}
\\usepackage{
  amsmath,
  amssymb,
  amsthm,
  blkarray,
  booktabs,
  breqn,
  fancyhdr,
  float,
  graphicx,
  hyperref,
  parskip,
  pgfplots,
  physics,
  setspace,
  tikz,
  xcolor
}
\\usetikzlibrary{shapes,arrows,positioning}
\\setlength{\\headheight}{15pt}
\\eqlinespacing=20pt
\\fancyhf{}
\\rhead{Garry Cairns}
\\rfoot{\\thepage}
\\pagestyle{fancy}
\\onehalfspacing{}
\\setlength\\parindent{0pt}
\\let\\oldhat\\hat{}
\\renewcommand{\\vec}[1]{\\mathbf{#1}}
\\renewcommand{\\hat}[1]{\\oldhat{\\mathbf{#1}}}
\\renewcommand\\thesection{Question \\arabic{section}}
\\renewcommand\\thesubsection{(\\alph{subsection})}
\\renewcommand\\thesubsubsection{(\\roman{subsubsection})}
[NO-DEFAULT-PACKAGES]"
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                 ("\\paragraph{%s}" . "\\paragraph*{%s}")
                 ("\\subparagraph{%s}" . "\\subparagraph*{%s}")
                 )
    )
                 '("book"
                   "\\documentclass{book}"
                 ("\\part{%s}" . "\\part*{%s}")
                 ("\\chapter{%s}" . "\\chapter*{%s}")
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                 )
  )
)

(use-package org-bullets
  :ensure t
  :config
  (add-hook 'org-mode-hook 'turn-off-auto-fill)
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
)

(use-package plantuml-mode
  :ensure t
  :config
  (add-to-list 'auto-mode-alist '("\\.uml\\'" . plantuml-mode))
  (setq plantuml-executable-path "/run/current-system/sw/bin/plantuml")
  (setq plantuml-default-exec-mode 'executable)
  (setq plantuml-output-type "svg")
)

;; Presentations

(use-package demo-it
  :ensure t
)

(use-package org-re-reveal
  :ensure t
  :config
  (setq org-re-reveal-revealjs-version "4.3.1")  ;; Specify the desired version
  (setq org-re-reveal-revealjs-url "https://cdn.jsdelivr.net/npm/reveal.js@4.3.1/")  ;; Set the CDN URL
)

(use-package org-re-reveal-citeproc
  :ensure t
)

(use-package org-re-reveal-ref
  :ensure t
)

(use-package org-tree-slide
  :ensure t
)

(use-package gdscript-mode
  :ensure t
)

(use-package go-mode
  :ensure t
)

(use-package haskell-mode
  :ensure t
  :config
  (setq haskell-process-wrapper-function
        (lambda (args) (apply 'nix-shell-command (nix-current-sandbox) args)))
  (setq haskell-compile-cabal-build-command "cabal new-build")
  (setq haskell-process-type "cabal new-repl")
)

(use-package markdown-mode
  :ensure t
)

(use-package nix-mode
  :ensure t
)

(use-package nixos-options
  :ensure t
)

(use-package protobuf-mode
  :ensure t
)

(use-package rainbow-mode
  :ensure t
)

(use-package rust-mode
  :ensure t
  :config
  (setq rust-format-on-save t)
)

(use-package scss-mode
  :ensure t
  :config
  (add-to-list 'auto-mode-alist '("\\.scss\\'" . scss-mode))
)

(use-package slime
  :ensure t
)

(use-package yaml-mode
  :ensure t
)

;; Maths

(use-package company-math
  :ensure t
)

(use-package tex-mode
  :ensure auctex
  :config
  (add-to-list 'company-backends 'company-math-symbols-latex)
  (setq latex-run-command "pdflatex")
)

;; Utils

(use-package restclient
  :ensure t
)

(use-package yasnippet
  :ensure t
  :config
  (setq yas-snippet-dirs '("~/nixos-config/snippets"))
  (yas-global-mode 1)
)

(provide 'init)
;;; init.el ends here
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("4c56af497ddf0e30f65a7232a8ee21b3d62a8c332c6b268c81e9ea99b11da0d3" "fee7287586b17efbfda432f05539b58e86e059e78006ce9237b8732fde991b4c" default))
 '(package-selected-packages
   '(solarized-light solarized-dark rainbow-delimiters nginx-mode flycheck ess dockerfile-mode yasnippet yaml-mode which-key use-package undo-tree try telephone-line sunny-day-theme solarized-theme slime scss-mode restclient rainbow-mode protobuf-mode projectile plantuml-mode org-tree-slide org-bullets notmuch nixos-options nix-mode markdown-mode magit leuven-theme key-chord haskell-mode go-mode general evil-escape evil elfeed-org demo-it company-math bluetooth auctex)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(fixed-pitch ((t (:family "IBM Plex Mono"))))
 '(org-block ((t (:inherit fixed-pitch))))
 '(org-code ((t (:inherit (shadow fixed-pitch)))))
 '(org-indent ((t (:inherit (org-hide fixed-pitch)))))
 '(org-latex ((t (:inherit (shadow fixed-pitch)))))
 '(org-table ((t (:inherit (shadow fixed-pitch)))))
 '(variable-pitch ((t (:family "IBM Plex Serif")))))
