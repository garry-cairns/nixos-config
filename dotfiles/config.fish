# system aliases
function backup
    if test (count $argv) -ne 2
        echo "Error: Exactly 2 arguments are required: the directory you're copying and the location to which you're copying it relative to /run/media/usb/."
        return 1  # Return a non-zero status to indicate an error
    end

    sudo rsync -rv $argv[1] /run/media/usb/$argv[2]

    # Confirm the backup was successful
    if test $status -eq 0
        echo "$argv[1] successfully backed up to /run/media/usb/$argv[2]"
    else
        echo "Failed to backup. Have you mounted a USB?"
    end
end

function mount_usb
    # Create a directory for the USB mount, including any parent dirs
    set -l mount_point "/run/media/usb"
    sudo mkdir -p $mount_point

    # Mount the USB drive
    sudo mount -o rw /dev/$argv $mount_point

    # Confirm the mount was successful
    if test $status -eq 0
        echo "USB drive mounted at $mount_point"
    else
        echo "Failed to mount USB drive"
    end
end

function prepare_project
    echo "use nix" > .envrc && direnv allow
end

# Only run this after you've already done a rebuil test at minimum and know your
# configuration works because it'll clear up old ones.
function unsafe_update
    sudo nixos-rebuild switch --upgrade
    sudo nix-store --optimize
    sudo nix-collect-garbage -d
end

function join_wifi
    nmcli --ask device wifi connect "$argv"
end
# end system aliases

# git aliases
function gitfilediff
    git diff --name-only --diff-filter=U
end
    
function gitgraph
    git log --all --decorate --oneline --graph
end
# end git aliases

# general config
# set -x GPG_TTY (tty) # prevents passphrase popups
direnv hook fish | source
# end general config
