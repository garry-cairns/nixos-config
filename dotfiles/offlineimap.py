#!/home/garry/.nix-profile/bin/python3
"""Extracts password from pass"""

import subprocess


def get_password(account):
    """Decrypts password for Mail/`account` from pass"""
    return subprocess.check_output("pass Mail/" + account, shell=True).splitlines()[0]
