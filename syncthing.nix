# Configuration for syncthing

{ config, pkgs, ... }:

{

  services = {

    syncthing = {
      dataDir = "/home/garry/.syncthing/";
      enable = true;
      group = "users";
      user = "garry";
    };

  };

}
