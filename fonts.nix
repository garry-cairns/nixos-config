{ config, pkgs, ... }:

{
  # Add some extra fonts
  fonts = {
    fontDir.enable = true;
    enableGhostscriptFonts = true;
    packages = with pkgs; [
      cooper-hewitt
      dejavu_fonts
      fira
      fira-mono
      font-awesome
      hasklig
      ibm-plex
      league-of-moveable-type
      source-code-pro
      source-sans-pro
      source-serif-pro
      ubuntu_font_family
    ];
  };
}
