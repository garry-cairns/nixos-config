{ config, pkgs, ... }:

{

  imports =
    [
      ./containers.nix
      ./desktop.nix
      ./dotfiles.nix
      ./fonts.nix
      ./hardware-configuration.nix
      ./mail.nix
      ./network.nix
      ./packages.nix
      ./sound.nix
      ./syncthing.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.initrd.luks.devices = {
    root = {
      device = "/dev/disk/by-uuid/8723e3c6-b437-4c37-8698-5d595d7e90d7";
      preLVM = true;
    };
  };

  i18n.defaultLocale = "en_GB.UTF-8";

  networking.hostName = "nixos"; # Define your hostname.
  networking.networkmanager.enable = true;  # Enables wireless support

  # Install and setup clamav
  services.clamav.daemon.enable = true;
  services.clamav.updater.enable = true;

  # Install emacs
  services.emacs = {
    install = true;
    defaultEditor = true;
    package = import ./emacs.nix { inherit pkgs; };
  };

  # Define user accounts. Don't forget to set a password with ‘passwd’.

  programs.direnv.enable = true;
  programs.fish.enable = true;
  programs.foot.enable = true;
  services.gnome.gnome-keyring.enable = true;

  users.users.garry = {
    isNormalUser = true;
    uid = 1000;
    home = "/home/garry";
    description = "Garry";
    extraGroups = [
      "wheel" "disk" "video"
      "audio" "pulse" "sound"
      "networkmanager" "systemd-journal"
      "docker" "bluetooth"
    ];
    shell = pkgs.fish;
  };
 
  # The NixOS release to be compatible with for stateful data such as databases.
  system.stateVersion = "23.11";

}
