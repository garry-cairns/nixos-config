# Configuration for containers

{ config, pkgs, ... }:

let
  kubeMasterIP = "10.1.1.2";
  kubeMasterHostName = "api.kube";
  kubeMasterAPIServerPort = 6443;
in

{

  ## kubernetes
  ## resolve master hostname
  #networking.extraHosts = "${kubeMasterIP} ${kubeMasterHostName}";

  ## packages for administration tasks
  #environment.systemPackages = with pkgs; [
  #  kind
  #  kompose
  #  kubectl
  #  kubernetes
  #  kubernetes-helm
  #];

  #services.kubernetes = {
  #  roles = ["master" "node"];
  #  masterAddress = kubeMasterHostName;
  #  apiserverAddress = "https://${kubeMasterHostName}:${toString kubeMasterAPIServerPort}";
  #  easyCerts = true;
  #  apiserver = {
  #    securePort = kubeMasterAPIServerPort;
  #    advertiseAddress = kubeMasterIP;
  #  };

  #  # use coredns
  #  addons.dns.enable = true;

  #  # needed if you use swap
  #  kubelet.extraOpts = "--fail-swap-on=false";
  #};

  # docker
  virtualisation = {
    docker.enable = true;
  };

}
