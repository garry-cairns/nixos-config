# This file configures offlineimap, notmuch and MSMTP.
#
# Some manual configuration is required the first time this is
# applied to set up credentials

{ config, lib, pkgs, ... }:

let offlineImapConfig = pkgs.writeText "offlineimaprc"
  (builtins.readFile ./dotfiles/offlineimaprc);

msmtpConfig = pkgs.writeText "msmtprc"
  (builtins.readFile ./dotfiles/msmtprc);

notmuchConfig = pkgs.writeText "notmuch-config"
  (builtins.readFile ./dotfiles/notmuch-config);

tagConfig = pkgs.writeText "notmuch-tags"
  (builtins.readFile ./dotfiles/.notmuch-tags);

notmuchIndex = pkgs.writeShellScriptBin "notmuch-index" ''
  echo "Indexing new mails in notmuch"

  # Index new mail
  ${pkgs.notmuch}/bin/notmuch new

  # Apply tags
  cat ${tagConfig} | ${pkgs.notmuch}/bin/notmuch tag --batch

  echo "Done indexing new mails"
'';

in {
  # Enable OfflineIMAP timer & service:
  systemd.user.timers.offlineimap = {
    description = "OfflineIMAP timer";
    wantedBy    = [ "timers.target" ];

    timerConfig = {
      Unit       = "offlineimap.service";
      OnCalendar = "*:0/2"; # every 2 minutes
      Persistent = "true"; # persist timer state after reboots
    };
  };

  systemd.user.services.offlineimap = {
    description = "OfflineIMAP service";
    path = with pkgs; [ offlineimap pass notmuch ];

    serviceConfig = {
      ExecStart       = "${pkgs.offlineimap}/bin/offlineimap -u syslog -o -c ${offlineImapConfig}";
      ExecStartPost   = "${notmuchIndex}/bin/notmuch-index";
      TimeoutStartSec = "2min";
      Type            = "oneshot";
    };
  };

  # Link configuration files to /etc/ (from where they will be linked
  # further):
  environment.etc = {
    "msmtprc".source = msmtpConfig;
    "notmuch-config".source = notmuchConfig;
    "offlineimaprc".source = offlineImapConfig;
  };
}
