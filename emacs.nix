{ pkgs ? import <nixpkgs> {} }:

let
  myEmacs = pkgs.emacs;
  emacsWithPackages = (pkgs.emacsPackagesFor myEmacs).emacsWithPackages;
in
  emacsWithPackages (epkgs: (with epkgs.elpaPackages; [
    bluetooth
    ement
    rainbow-mode
  ]) ++

  # MELPA packages:
  (with epkgs.melpaPackages; [
    code-cells
    company-math
    demo-it
    direnv
    dockerfile-mode
    elfeed
    elfeed-org
    ess
    evil
    flycheck
    general
    go-mode
    haskell-mode
    key-chord
    magit
    markdown-mode
    nginx-mode
    nixos-options
    oer-reveal
    org-bullets
    org-re-reveal
    org-re-reveal-citeproc
    org-re-reveal-ref
    org-tree-slide
    plantuml-mode
    projectile
    rainbow-delimiters
    restclient
    rust-mode
    scss-mode
    slime
    solarized-theme
    telephone-line
    yaml-mode
    yasnippet
    which-key
  ])
)
