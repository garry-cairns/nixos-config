{ config, pkgs, lib, ...}:

{
  environment = {
    etc = {
      "fish/config.fish".source         = lib.mkForce ./dotfiles/config.fish;
      gitconfig.source                  = ./dotfiles/gitconfig;
      "sway/config".source              = lib.mkForce ./dotfiles/sway.config;
      "xdg/foot/foot.ini".source            = lib.mkForce ./dotfiles/foot.ini;
      "xdg/waybar/config".source        = lib.mkForce ./dotfiles/waybar.config;
      "xdg/waybar/style.css".source     = lib.mkForce ./dotfiles/waybar.css;
    };
  };
}
