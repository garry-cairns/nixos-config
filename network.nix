{ config, pkgs, ... }:

# Remember to update each config with how to retrieve connection details
{
  services.openvpn.servers = {
    homeVPN = {
      # start with `sudo systemctl start openvpn-homeVPN.service`
      # stop with `sudo systemctl stop openvpn-homeVPN.service`
      autoStart = false;
      config = '' config /home/garry/nixos-config/homeVPN.conf '';
    };
    sloveniaVPN = {
      autoStart = false;
      config = '' config /home/garry/nixos-config/sloveniaVPN.conf '';
    };
    swedenVPN = {
      autoStart = false;
      config = '' config /home/garry/nixos-config/swedenVPN.conf '';
    };
    usVPN = {
      autoStart = false;
      config = '' config /home/garry/nixos-config/usVPN.conf '';
    };
  };

  # packages for network
  environment.systemPackages = with pkgs; [
    openvpn
  ];

}
