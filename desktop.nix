# Configuration for the desktop environment

{ config, pkgs, ... }:

{

  location = {
    latitude = 55.8642;
    longitude = 4.2518;
  };

  programs.gnupg.agent = {
    enable = true;
    pinentryPackage = pkgs.pinentry-tty;
  };

  programs.sway.enable = true;
  programs.sway.extraPackages = with pkgs; [
    brightnessctl
    greetd.tuigreet
    grim
    numix-solarized-gtk-theme
    pw-volume
    rofi-wayland
    slurp
    swayidle
    swayimg
    swaylock
    wf-recorder
    wl-clipboard
    wl-gammarelay-rs
    yazi
  ];
  programs.waybar.enable = true;

  security.pam.services.kwallet = {
    name = "kwallet";
    enableKwallet = true;
  };

  services = {
    greetd = {
      enable = true;
      settings = {
       default_session.command = ''
        ${pkgs.greetd.tuigreet}/bin/tuigreet \
          --time \
          --asterisks \
          --user-menu \
          --cmd sway
      '';
      };
    };

    libinput = {
      enable = true;
      touchpad = {
        scrollMethod = "edge";
        tapping = false;
      };
    };

    openssh.enable = true;
  };

  # Set your time zone.
  time.timeZone = "Europe/London";

  xdg.portal.enable = true;
  xdg.portal.configPackages = with pkgs; [
    xdg-desktop-portal-wlr
  ];

}
